package com.devcamp.springrestapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.springrestapi.Exceptions.DepartmentNotFoundException;
import com.devcamp.springrestapi.models.Department;
import com.devcamp.springrestapi.models.Staff;
import com.devcamp.springrestapi.services.DepartmentService;
import com.devcamp.springrestapi.services.StaffService;

@RestController
@CrossOrigin
@RequestMapping("/api/v1")
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private StaffService staffService;

    @GetMapping("/departments")
    public ArrayList<Department> getAllDepartments() {
        return departmentService.getDepartments();
    }

    @GetMapping("/departments/{departmentId}")
    public ResponseEntity<Object> getDepartmentById(@PathVariable("departmentId") int id) {
        try {
            Department department = departmentService.getDepartmentById(id);
            return ResponseEntity.ok(department);
        } catch (DepartmentNotFoundException ex) {

            String errorMessage = "Department not found with ID: " + id;
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorMessage);
        }
    }

    @GetMapping("/staffs")
    public ArrayList<Staff> getStaff() {
        return staffService.getStaff();
    }

    @GetMapping("/staffs-by-age")
    public ArrayList<Staff> getStaffByAge(@RequestParam(defaultValue = "") int age) {
        return staffService.getStaffByAge(age);
    }

    @GetMapping("/departments-by-age")
    public ArrayList<Department> getDepartmentByAge(@RequestParam(defaultValue = "") float averageAge) {
        return departmentService.getDepartmentsByAverageAge(averageAge);
    }

}
