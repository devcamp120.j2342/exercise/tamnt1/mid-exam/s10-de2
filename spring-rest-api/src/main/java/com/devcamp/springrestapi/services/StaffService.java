package com.devcamp.springrestapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.springrestapi.models.Department;
import com.devcamp.springrestapi.models.Staff;

@Service
public class StaffService {
    @Autowired
    DepartmentService departmentService;

    public ArrayList<Staff> getStaff() {
        ArrayList<Department> departments = departmentService.getDepartments();
        ArrayList<Staff> staffList = new ArrayList<>();
        for (Department department : departments) {
            staffList.addAll(department.getStaffs());
        }

        return staffList;
    }

    public ArrayList<Staff> getStaffByAge(int age) {
        ArrayList<Staff> staffList = getStaff();
        ArrayList<Staff> staffListByAge = new ArrayList<>();

        for (Staff staff : staffList) {
            if (staff.getAge() > age && age > 0) {
                staffListByAge.add(staff);
            }
        }
        return staffListByAge;
    }
}
