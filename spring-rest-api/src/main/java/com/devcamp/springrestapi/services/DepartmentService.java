package com.devcamp.springrestapi.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.springrestapi.Exceptions.DepartmentNotFoundException;
import com.devcamp.springrestapi.models.Department;
import com.devcamp.springrestapi.models.Staff;

@Service
public class DepartmentService {

    public ArrayList<Department> getDepartments() {
        ArrayList<Department> departments = new ArrayList<>();
        // Department 1
        ArrayList<Staff> staffList1 = new ArrayList<>();
        staffList1.add(new Staff(1, "Staff 1", 25));
        staffList1.add(new Staff(2, "Staff 2", 30));
        staffList1.add(new Staff(3, "Staff 3", 28));

        Department department1 = new Department(1, "Department 1", "Address 1", staffList1);
        departments.add(department1);

        // Department 2
        ArrayList<Staff> staffList2 = new ArrayList<>();
        staffList2.add(new Staff(4, "Staff 4", 32));
        staffList2.add(new Staff(5, "Staff 5", 27));
        staffList2.add(new Staff(6, "Staff 6", 29));

        Department department2 = new Department(2, "Department 2", "Address 2", staffList2);
        departments.add(department2);

        // Department 3
        ArrayList<Staff> staffList3 = new ArrayList<>();
        staffList3.add(new Staff(7, "Staff 7", 31));
        staffList3.add(new Staff(8, "Staff 8", 26));
        staffList3.add(new Staff(9, "Staff 9", 33));

        Department department3 = new Department(3, "Department 3", "Address 3", staffList3);
        departments.add(department3);

        return departments;

    }

    public Department getDepartmentById(int departmentId) {
        ArrayList<Department> departments = getDepartments();
        for (Department department : departments) {
            if (department.getId() == departmentId) {
                return department;
            }
        }
        throw new DepartmentNotFoundException("Department not found");
    }

    public ArrayList<Department> getDepartmentsByAverageAge(float averageAge) {
        ArrayList<Department> allDepartments = getDepartments();
        ArrayList<Department> departmentsWithGreaterAverageAge = new ArrayList<>();

        for (Department department : allDepartments) {
            ArrayList<Staff> staffList = department.getStaffs();
            int totalAge = 0;

            for (Staff staff : staffList) {
                totalAge += staff.getAge();
            }

            float departmentAverageAge = (float) totalAge / staffList.size();

            if (departmentAverageAge > averageAge) {
                departmentsWithGreaterAverageAge.add(department);
            }
        }

        return departmentsWithGreaterAverageAge;
    }

}
