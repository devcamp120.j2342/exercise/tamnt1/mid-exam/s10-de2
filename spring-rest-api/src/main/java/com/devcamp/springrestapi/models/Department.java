package com.devcamp.springrestapi.models;

import java.util.ArrayList;

public class Department {
    private int id;
    private String name;
    private String address;
    private ArrayList<Staff> staffs;

    // Constructors

    public Department() {
        this.id = 0;
        this.name = "";
        this.address = "";
        this.staffs = new ArrayList<>();
    }

    public Department(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.staffs = new ArrayList<>();
    }

    public Department(int id, String name, String address, ArrayList<Staff> staffs) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.staffs = staffs;
    }

    // Getters

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public ArrayList<Staff> getStaffs() {
        return staffs;
    }

    // Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setStaffs(ArrayList<Staff> staffs) {
        this.staffs = staffs;
    }

    // Methods

    public float getAverageAge() {
        int totalAge = 0;
        for (Staff staff : staffs) {
            totalAge += staff.getAge();
        }
        if (staffs.size() > 0) {
            return (float) totalAge / staffs.size();
        } else {
            return 0;
        }
    }
}
